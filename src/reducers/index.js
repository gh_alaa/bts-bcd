import { combineReducers } from 'redux';

const userInfoReducer = (state = false, action) => {
  switch (action.type) {
    case 'GET_USER':
      return action.payload.user.PersonalInfosRow;
    case 'POP_USER':
      return false;
    default:
      return state;
  }
};

const accountsReducer = (state = false, action) => {
  switch (action.type) {
    case 'GET_USER':
      return action.payload.user.Accounts;
    case 'POP_USER':
      return false;
    default:
      return state;
  }
};

const userReducer = (state = false, action) => {
  switch (action.type) {
    case 'GET_USER':
      return action.payload.user.UsersRow;
    case 'POP_USER':
      return false;
    default:
      return state;
  }
};


export default combineReducers({
  user: userReducer,
  userInfo: userInfoReducer,
  accounts: accountsReducer,
});
