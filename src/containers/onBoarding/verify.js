import React, { Component } from 'react';
import {
  AsyncStorage, I18nManager, Image, StyleSheet, Text, View, StatusBar, Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import axios from 'axios';
import { parseString } from 'react-native-xml2js';
import BButton from '../../components/Buttons';
import { BSecureTextInput } from '../../components/TextInputs';
import serverLink from '../../../configs';
import getXmlData from '../../../helper';


const Variables = I18nManager.isRTL
  ? {
    SecureAccess: 'تأمين الدخول',
    confirm: 'لتأكيد رقم هاتفك',
    confirmationMsg: 'أدخل الرمز المكون من ٤ آرقام والذي آرسلناه للرقم',
    confirmIt: 'استمـرار'
  }
  : {
    SecureAccess: 'Secure Access',
    confirm: 'Confirm mobile number',
    confirmationMsg: 'Enter the 4-digit code we sent to',
    confirmIt: 'Confirm'
  };


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fee100',
    flexDirection: 'column',
    paddingTop: 20,
    minHeight: '110%'
  },
  title: {
    fontSize: 25,
    color: '#006d33',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.65)',
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Bold' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
    padding: 7,
  },
  subtitle: {
    fontSize: 20,
    color: 'black',
    padding: 15,
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  number: {
    fontSize: 24,
    color: 'black',
    padding: 10,
    width: '85%',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  TextInputContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 8,
    paddingBottom: 8
  },
  loginError: {
    fontSize: 12,
    color: 'red',
    paddingTop: 5,
  }
});


class PhoneScreen extends Component {
  static navigationOptions = { header: null };

  constructor() {
    super();
    this.state = {
      code: '',
      errorMessage: false

    };
    this.VerifyUser = this.VerifyUser.bind(this);
  }

  VerifyUser() {
    const that = this;
    const xmls = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <VerifyUser xmlns="http://tempuri.org/">
      <Phone>${that.props.navigation.state.params}</Phone>
      <VerificationCode>${that.state.code}</VerificationCode>
    </VerifyUser>
  </soap:Body>
</soap:Envelope>

        `;
    axios.post(serverLink, xmls, { headers: { 'Content-Length': '255', 'Content-Type': 'text/xml', SOAPAction: 'http://tempuri.org/VerifyUser' } }).then((res) => {
      parseString(res.data.toString(), function VerifyUserData(err, result) {
        const serverData = getXmlData(result, 'VerifyUser');
        if (serverData.ErrorMessage === '') {
          AsyncStorage.setItem('@BCD:verified', JSON.stringify(true)).catch(errM => this.showAlert('Error', errM.message));
          that.props.navigation.navigate('Login');
        } else {
          that.setState({ errorMessage: serverData.ErrorMessage });
        }
      });
    }).catch((err) => { console.error(err); });
  }

  render() {
    const { navigation } = this.props;
    const { errorMessage } = this.state;
    return (
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        enableOnAndroid
        extraScrollHeight={20}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
      >
        {Platform.OS === 'ios' ? <View style={{ width: '100%', height: 40, backgroundColor: '#fee100' }} /> : <StatusBar backgroundColor="#fee100" barStyle="dark-content" /> }
        <View style={{ padding: 5, paddingBottom: 10 }}>
          <Image
            style={{ height: 89.116, width: 82.032 }}
            source={require('../../../assets/logoOnboard.png')}
          />
        </View>
        <Text style={styles.title}>{Variables.SecureAccess}</Text>
        {errorMessage && <Text style={styles.loginError}>{errorMessage}</Text>}
        <Text style={[styles.subtitle, { paddingBottom: 0 }]}>{Variables.confirm}</Text>
        <Text style={[styles.subtitle, { padding: 0 }]}>{Variables.confirmationMsg}</Text>
        <Text style={styles.number}>{navigation.state.params}</Text>
        <BSecureTextInput keyboard="numeric" onChange={text => this.setState({ code: text })} />
        <BButton buttonText={Variables.confirmIt} onPress={this.VerifyUser} />
      </KeyboardAwareScrollView>
    );
  }
}


export default PhoneScreen;
