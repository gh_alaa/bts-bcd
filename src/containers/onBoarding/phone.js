import React, { Component } from 'react';
import {
  Dimensions, I18nManager, Image, StyleSheet, Text, View, StatusBar, Platform
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import axios from 'axios';
import { parseString } from 'react-native-xml2js';
import BButton from '../../components/Buttons';
import { BTextInput } from '../../components/TextInputs';
import serverLink from '../../../configs';
import getXmlData from '../../../helper';

const ArabicFont = Platform.OS === 'ios'
  ? 'GE SS Two'
  : 'GE-SS-Two-Bold';

const Font = I18nManager.isRTL
  ? ArabicFont
  : 'Arial';

const Variables = I18nManager.isRTL
  ? {
    registration: 'تسجيل الجهاز',
    enterPhone: 'ادخل رقم الهاتف',
    continue: 'استمـرار',
    errorMessage: 'هناك خطأ ما'
  }
  : {
    registration: 'Device Registration',
    enterPhone: 'Please enter phone number',
    continue: 'Continue',
    errorMessage: 'Something went wrong'
  };


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fee100',
    flexDirection: 'column',
    paddingTop: 20,
  },
  title: {
    fontSize: 25,
    color: '#006d33',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.65)',
    width: '85%',
    textAlign: 'center',
    padding: 7,
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
    fontFamily: Font,
  },
  subtitle: {
    fontSize: 20,
    color: 'black',
    padding: 15,
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
    fontFamily: Font,
  },
  TextInputContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginError: {
    fontSize: 12,
    color: 'red',
    paddingTop: 5,
  },
  secondSplash: {
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 100,
    top: 0,
    width: Dimensions.get('window').width,
    position: 'absolute',
    backgroundColor: '#fee100',
    height: Dimensions.get('window').height,
    paddingBottom: 30,
  }
});


class PhoneScreen extends Component {
  static navigationOptions = { header: null };

  constructor() {
    super();
    this.state = {
      phone: '',
      errorMessage: false,
      render: true

    };
    this.regDevice = this.regDevice.bind(this);
  }

  componentDidMount() {
    // Start counting when the page is loaded
    this.timeoutHandle = setTimeout(() => {
      this.setState({ render: false });
    }, 5000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }

  regDevice() {
    const that = this;
    const { phone } = this.state;
    const xmls = `<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
        <RegDevice xmlns="http://tempuri.org/">
          <Phone>${phone}</Phone>
        </RegDevice>
      </soap:Body>
    </soap:Envelope>
          `;
    axios.post(serverLink, xmls, { headers: { 'Content-Length': '255', 'Content-Type': 'text/xml', SOAPAction: 'http://tempuri.org/RegDevice' } }).then((res) => {
      parseString(res.data.toString(), (err, result) => {
        const serverData = getXmlData(result, 'RegDevice');
        if (serverData.ErrorMessage === '') {
          that.props.navigation.navigate('Verify', that.state.phone);
        } else {
          that.setState({ errorMessage: serverData.ErrorMessage });
        }
      });
    }).catch(() => { that.setState({ errorMessage: Variables.errorMessage }); });
  }

  render() {
    const { errorMessage, render } = this.state;
    return (
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        extraScrollHeight={45}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
      >
        {Platform.OS === 'ios' ? <View style={{ width: '100%', height: 40, backgroundColor: '#fee100' }} /> : <StatusBar backgroundColor="#fee100" barStyle="dark-content" /> }
        <View style={{ padding: 5, paddingBottom: 10 }}>
          <Image
            style={{ height: 89.116, width: 82.032 }}
            source={require('../../../assets/logoOnboard.png')}
          />
        </View>
        <Text style={styles.title}>{Variables.registration}</Text>
        {errorMessage && <Text style={styles.loginError}>{errorMessage}</Text>}
        <Text style={styles.subtitle}>{Variables.enterPhone}</Text>
        <View style={styles.TextInputContainer}>
          <BTextInput keyboard="phone-pad" onChange={text => this.setState({ phone: text })} />
        </View>
        <BButton buttonText={Variables.continue} onPress={this.regDevice} />
        {render
            && (
            <View style={styles.secondSplash}>
              <View style={{
                borderColor: 'black', borderWidth: 1, paddingLeft: 10, paddingRight: 10, left: 0, righ: 0
              }}
              >
                <Text style={{
                  fontSize: 16, color: 'black', fontFamily: ArabicFont, position: 'absolute', top: -10, left: 8, paddingBottom: 5, backgroundColor: '#fee100'
                }}
                >
مرحبا بكم في تطبيق
                </Text>
                <Text style={{
                  color: '#006d33', fontSize: 80, fontFamily: 'Arial', fontWeight: '900'
                }}
                >
BCD
                </Text>
              </View>
              <Text style={{
                fontSize: 16, color: 'black', fontFamily: 'Arial', fontWeight: '500'
              }}
              >
BTS Mobile Payment
              </Text>
            </View>
            )
          }

      </KeyboardAwareScrollView>
    );
  }
}


export default PhoneScreen;
