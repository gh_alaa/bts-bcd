import React, { Component } from 'react';
import {
  AsyncStorage,
  ScrollView,
  I18nManager,
  Image,
  StyleSheet,
  Text,
  View,
  StatusBar,
  Platform,
  TouchableOpacity
} from 'react-native';
import axios from 'axios';
import { parseString } from 'react-native-xml2js';
import { connect } from 'react-redux';
import BButton from '../../components/Buttons';
import { BTextInput, BSecureTextInput } from '../../components/TextInputs';
import serverLink from '../../../configs';
import getXmlData from '../../../helper';
import { GetUser } from '../../actions';

const Variables = I18nManager.isRTL
  ? {
    Login: 'تسجيل الدخول',
    Username: 'اسم المستخدم أو رقم الحساب بدون فواصل (13 خانه)',
    Password: 'كلمة المرور ',
    loginButton: 'تسجيل الدخول',
    Forget: 'هل نسيت كلمة المرور ',
    newUser: 'مستخدم جديد'
  }
  : {
    Login: 'Login',
    Username: 'Username or account number without commas (13 digits)',
    Password: 'Password',
    loginButton: 'Login',
    Forget: 'Did you forget your password?',
    newUser: 'New User'
  };


const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fee100',
    flexDirection: 'column',
    paddingTop: 20,
    minHeight: '100%'
  },
  title: {
    fontSize: 25,
    color: '#006d33',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.65)',
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Bold' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
    padding: 7,
  },
  subtitle: {
    fontSize: 17,
    color: 'black',
    paddingTop: 15,
    paddingBottom: 5,
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  TextInputContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 8,
    paddingBottom: 8
  },
  loginError: {
    fontSize: 12,
    color: 'red',
    paddingTop: 5,
  }
});


class LoginContainer extends Component {
  static navigationOptions = { header: null };

  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      errorMessage: false
    };
    this.login = this.login.bind(this);
  }

  login() {
    const that = this;
    const { username, password } = this.state;
    const xmls = `<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Body>
                    <Login xmlns="http://tempuri.org/">
                    <UserId>${username}</UserId>
                    <Password>${password}</Password>
                    </Login>
                    </soap:Body>
                    </soap:Envelope>
                    `;
    axios.post(serverLink, xmls, { headers: { 'Content-Length': '255', 'Content-Type': 'text/xml', SOAPAction: 'http://tempuri.org/Login' } }).then((res) => {
      parseString(res.data.toString(), function SaveData(err, result) {
        const serverData = getXmlData(result, 'Login');
        if (serverData.ErrorMessage === '') {
          AsyncStorage.setItem('@BCD:loggedIn', JSON.stringify(true)).catch(errM => this.showAlert('Error', errM.message));
          AsyncStorage.setItem('@BCD:fullName', serverData.PersonalInfosRow['Full Name']).catch(errM => this.showAlert('Error', errM.message));
          AsyncStorage.setItem('@BCD:phone', serverData.PersonalInfosRow['Phone Number']).catch(errM => this.showAlert('Error', errM.message));
          AsyncStorage.setItem('@BCD:active', serverData.PersonalInfosRow.Active).catch(errM => this.showAlert('Error', errM.message));
          AsyncStorage.setItem('@BCD:thresold', serverData.PersonalInfosRow['Request confirmation on amount above']).catch(errM => this.showAlert('Error', errM.message));
          AsyncStorage.setItem('@BCD:recidUser', serverData.UsersRow.RecId_Users).catch(errM => this.showAlert('Error', errM.message));
          AsyncStorage.setItem('@BCD:userid', serverData.UsersRow.UserId).catch(errM => this.showAlert('Error', errM.message));
          AsyncStorage.setItem('@BCD:currency', serverData.UsersRow.Currency).catch(errM => this.showAlert('Error', errM.message));
          AsyncStorage.setItem('@BCD:accounts', serverData.UsersRow.Accounts).catch(errM => this.showAlert('Error', errM.message));
          const user = { ...serverData };
          that.props.onLogin({ user });
          that.props.navigation.navigate('App');
        } else {
          that.setState({ errorMessage: serverData.ErrorMessage });
        }
      });
    }).catch((err) => { console.error(err); /* eslint-disable-line */});
  }

  render() {
    console.log(this.props);
    const { errorMessage } = this.state;
    return (
      <ScrollView
        keyboardShouldPersistTaps="always"
        contentContainerStyle={styles.container}
      >
        {Platform.OS === 'ios' ? <View style={{ width: '100%', height: 40, backgroundColor: '#fee100' }} /> : <StatusBar backgroundColor="#fee100" barStyle="dark-content" /> }
        <View style={{ padding: 5, paddingBottom: 10 }}>
          <Image
            style={{ height: 89.116, width: 82.032 }}
            source={require('../../../assets/logoOnboard.png')}
          />
        </View>
        <Text style={styles.title}>{Variables.Login}</Text>
        {errorMessage && <Text style={styles.loginError}>{errorMessage}</Text>}
        <Text style={styles.subtitle}>{Variables.Username}</Text>
        <BTextInput onChange={username => this.setState({ username })} />
        <Text style={[styles.subtitle, { padding: 5, paddingTop: 10 }]}>{Variables.Password}</Text>
        <BSecureTextInput onChange={password => this.setState({ password })} />
        <BButton buttonText={Variables.loginButton} onPress={this.login} />
        <TouchableOpacity>
          <Text style={[styles.subtitle, { fontSize: 15, paddingTop: 5, paddingBottom: 40 }]}>
            {Variables.Forget}
          </Text>
        </TouchableOpacity>
        <BButton style={{ position: 'absolute', bottom: 10 }} buttonText={Variables.newUser} />

      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  onLogin: (user) => {
    dispatch(GetUser(user));
  },
});

const LoginScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer);

export default LoginScreen;
