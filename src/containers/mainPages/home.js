/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  Dimensions, TouchableOpacity, Linking,
  I18nManager, StyleSheet, View, Platform, StatusBar, Image
} from 'react-native';
import { connect } from 'react-redux';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import Footer from '../../components/Footer';
import { Category, Category2 } from '../../components/Category';
import InfoBar from '../../components/InfoBar';
import myFont from '../../../assets/fonts/selection.json';
import { PopUser } from '../../actions';

const Icon = createIconSetFromIcoMoon(myFont);


const maxSize = 100;
const finalSize = (Dimensions.get('window').height - 320) / 4;
const sizes = (finalSize < maxSize) ? finalSize : maxSize;

const Variables = I18nManager.isRTL
  ? {
    myAccount: 'حسابي',
    internalTransfer: 'تحويل داخلي',
    payMe: 'ادفع لي',
    myCards: 'كروتي',
    pos: 'نقاط البيع',
    cards: 'بطاقات',
    customerService: 'خدمة الزبائن',
    bookForMe: 'إحجز لي',
    forms: 'نماذج',
    otherServices: 'خدمات أخرى'
  }
  : {
    myAccount: 'My Account',
    internalTransfer: 'Internal Transfer',
    payMe: 'Pay Me',
    myCards: 'My Cards',
    pos: 'P.O.S',
    cards: 'Cards',
    customerService: 'Customer Service',
    bookForMe: 'Book For Me',
    forms: 'Forms',
    otherServices: 'Other Services'
  };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'rgba(0,147,75,0.02)',
    flexDirection: 'column',
    paddingTop: 20,
    paddingBottom: 100,
  },
  row: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    width: '95%',
    maxWidth: 320,
  },
});


type Props = {};
class HomeScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    title: 'Home',
    headerStyle: {
      backgroundColor: '#fee100',
      height: 33,
      marginTop: 20,
      marginBottom: 5,
      shadowOpacity: 0,
      shadowOffset: {
        height: 0,
      },
      shadowRadius: 0,
      elevation: 0,
    },
    headerTitle: (
      <View style={{
        position: 'absolute', padding: 5, width: '100%', alignItems: 'center', justifyContent: 'center'
      }}
      >
        <Image style={{ width: 70, height: 70 }} source={require('../../../assets/headerLogo.png')} />
      </View>),
    headerTintColor: '#fff',
    headerTitleStyle: {
      alignSelf: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      width: '100%',
      display: 'flex',
    },
    headerRight: <TouchableOpacity style={{ paddingRight: ((Dimensions.get('window').width - 320) / 2) + 5 }}><FAIcon name="list" size={25} color="#006d33" /></TouchableOpacity>,
    headerLeft: (
      <TouchableOpacity
        onPress={() => {
          navigation.state.params.handleFilter();
          navigation.navigate('Auth');
        }}
        style={{ paddingRight: 20, paddingLeft: ((Dimensions.get('window').width - 320) / 2) + 5 }}
      >
        <FAIcon name="lock" size={25} color="#006d33" />
      </TouchableOpacity>),
  });

  componentDidMount() {
    const { navigation, onLogout } = this.props;
    const logging = () => { onLogout([]); };
    navigation.setParams({
      handleFilter: logging
    });
    console.log(this.props);
  }

  render() {
    const { navigation } = this.props;
    const iconSize = sizes * 0.58;
    return (
      <View style={styles.container}>
        {Platform.OS === 'android' && <StatusBar backgroundColor="#006d33" barStyle="dark-content" /> }
        <View style={[styles.row, { paddingLeft: 5, paddingRight: 5 }]}>
          <InfoBar />
        </View>
        <View style={[styles.row, { paddingTop: 10 }]}>
          <Category icon={<Icon name="account" size={iconSize} color="black" />} title={Variables.myAccount} />
          <Category icon={<Icon name="transfer" size={iconSize} color="black" />} onPress={() => navigation.navigate('Transfer')} title={Variables.internalTransfer} />
          <Category icon={<Icon name="pay" size={iconSize * 0.75} color="black" />} onPress={() => navigation.navigate('PayMe')} title={Variables.payMe} />
        </View>
        <View style={styles.row}>
          <Category icon={<Icon name="sim" size={iconSize * 0.7} color="black" />} title={Variables.myCards} />
          <Category icon={<Icon name="pos" size={iconSize * 0.75} color="black" />} title={Variables.pos} />
          <Category icon={<Icon name="cards" size={iconSize * 0.75} color="black" />} title={Variables.cards} />
        </View>
        <View style={styles.row}>
          <Category onPress={() => Linking.openURL('tel:00218213621994')} icon={<Icon name="customer" size={iconSize * 0.75} color="black" />} title={Variables.customerService} />
          <Category icon={<Icon name="book" size={iconSize * 0.7} color="black" />} title={Variables.bookForMe} />
          <Category icon={<Icon name="form" size={iconSize * 0.6} color="black" />} title={Variables.forms} />
        </View>
        <View style={styles.row}>
          <Category2 onPress={() => navigation.navigate('OtherServices')} title={Variables.otherServices} />
        </View>
        <Footer />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  onLogout: (userdata) => {
    dispatch(PopUser(userdata));
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
