import React, { Component } from 'react';
import {
  AsyncStorage, I18nManager, Image, StyleSheet, Text, View, StatusBar, Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import QRCode from 'react-native-qrcode-svg';
import BButton from '../../components/Buttons';


const Variables = I18nManager.isRTL
  ? {
    qrCode: 'ادفعلي عن طريق رمز ال QR',
    enterAmount: 'الرجاء ادخال المبلغ',
    next: 'استمـرار'
  }
  : {
    qrCode: 'Edfali using QR code',
    enterAmount: 'Please enter the amount',
    next: 'Done'
  };


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fee100',
    flexDirection: 'column',
    paddingTop: 20,
    minHeight: '110%'
  },
  title: {
    fontSize: 25,
    color: '#006d33',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.65)',
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Bold' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
    padding: 7,
  },
  subtitle: {
    fontSize: 20,
    color: 'black',
    paddingTop: 15,
    paddingBottom: 15,
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  number: {
    fontSize: 24,
    color: 'black',
    padding: 10,
    width: '85%',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  TextInputContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 8,
    paddingBottom: 8
  },
  rowPress: {
    flexDirection: 'row',
    padding: 0,
    alignItems: 'center',
    width: '85%',
  },
  changeNumberText: {
    fontSize: 18,
    color: 'black',
    padding: 5,
    paddingBottom: 0,
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  }
});


class PhoneScreen extends Component {
  static navigationOptions = { header: null };

  constructor() {
    super();
    this.state = {
      Currency: '',
      userid: ''
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('@BCD:currency').then((Currency) => {
      this.setState({ Currency });
    });
    AsyncStorage.getItem('@BCD:userid').then((userid) => {
      this.setState({ userid });
    });
  }

  render() {
    const { navigation } = this.props;
    const { userid, Currency } = this.state;
    const Amount = navigation.state.params;
    const DATA = `{"Amount": "${Amount}","userid": "${userid}","Currency": "${Currency}"}`;
    return (
      <KeyboardAwareScrollView
        enableOnAndroid
        extraScrollHeight={20}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
      >
        {Platform.OS === 'ios' ? <View style={{ width: '100%', height: 40, backgroundColor: '#fee100' }} /> : <StatusBar backgroundColor="#fee100" barStyle="dark-content" /> }
        <View style={{ padding: 5, paddingBottom: 10 }}>
          <Image
            style={{ height: 89.116, width: 82.032 }}
            source={require('../../../assets/logoOnboard.png')}
          />
        </View>
        <Text style={styles.title}>{Variables.qrCode}</Text>
        <View style={{ padding: 30 }}>
          <QRCode
            value={DATA.toString()}
            size={200}
            backgroundColor="transparent"
          />
        </View>
        <BButton buttonText={Variables.next} onPress={() => { navigation.navigate('Home'); }} />
      </KeyboardAwareScrollView>
    );
  }
}


export default PhoneScreen;
