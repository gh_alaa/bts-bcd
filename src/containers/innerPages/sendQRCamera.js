import React, { Component } from 'react';
import { View } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';


class PhoneScreen extends Component {
  static navigationOptions = { header: null };

  onSuccess(e) {
    const { navigation } = this.props;
    if (e.data) {
      const getData = JSON.parse(e.data);

      if (parseInt(getData.Amount, 10) > 0) {
        navigation.navigate('SendQRVerify', { number: navigation.state.params, data: getData });
      } else {
        navigation.navigate('SendQRAmount', { number: navigation.state.params, data: getData });
      }
    } else {
      navigation.navigate('SendQRVerify', { number: navigation.state.params, data: e.data });
    }
  }

  render() {
    return (
      <QRCodeScanner
        containerStyle={{ backgroundColor: '#fee100' }}
        onRead={e => this.onSuccess(e)}
        showMarker
        cameraStyle={{ height: '100%' }}
        reactivateTimeout={3}
        customMarker={(
          <View style={{
            width: 200, height: 200, borderColor: '#fee100', borderRadius: 4, borderWidth: 1
          }}
          />
      )}
      />
    );
  }
}


export default PhoneScreen;
