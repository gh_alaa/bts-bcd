/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  FlatList, Dimensions, TouchableOpacity, I18nManager, StyleSheet, View, Platform, StatusBar, Image
} from 'react-native';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import InfoBar from '../../components/InfoBar';
import SubCategory from '../../components/subCategory';
import Footer from '../../components/Footer';


const Variables = I18nManager.isRTL
  ? {
    internalTransfer: 'قائمة حـولاتـي الداخلية',
    visa: 'كشف حساب بطاقة فيزا',
    curruncy: 'أسعار خدمات المصرف',
    payme: 'قائمة مشتريات إدفع لي',
    pos: 'قائمة مشتريات P.O.S',
    call: 'عناويننا وأرقام هواتفنا',
    share: 'مشاركة التطبيق',
    edit: 'تعديل البينات',
    about: 'عن التطبيق',
  }
  : {
    internalTransfer: 'Internal Transactions List',
    visa: 'Visa Card Statement',
    curruncy: 'Bank Services Rate',
    payme: 'Edfali purchases',
    pos: 'P.O.S purchases',
    call: 'Locations',
    share: 'Share Application',
    edit: 'Edit Information',
    about: 'About This Application',
  };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgba(0,147,75,0.02)',
    flexDirection: 'column',
    paddingTop: 20,
    paddingBottom: 120,
  },
  row: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    width: '95%',
    maxWidth: 320,
  },
  buttonsContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    padding: 5,
  }
});


type Props = {};
export default class App extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    title: 'Home',
    headerStyle: {
      backgroundColor: '#fee100',
      height: 33,
      marginTop: 20,
      marginBottom: 5,
      shadowOpacity: 0,
      shadowOffset: {
        height: 0,
      },
      shadowRadius: 0,
      elevation: 0,
    },
    headerTitle: (
      <View style={{
        position: 'absolute', padding: 5, width: '100%', alignItems: 'center', justifyContent: 'center'
      }}
      >
        <Image style={{ width: 70, height: 70 }} source={require('../../../assets/headerLogo.png')} />
      </View>),
    headerTintColor: '#fff',
    headerTitleStyle: {
      alignSelf: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      width: '100%',
      display: 'flex',
    },
    headerRight: <TouchableOpacity style={{ paddingRight: ((Dimensions.get('window').width - 320) / 2) + 5 }}><FAIcon name="list" size={21} color="#006d33" /></TouchableOpacity>,
    headerLeft: <TouchableOpacity onPress={() => navigation.goBack()} style={{ paddingRight: 20, paddingLeft: ((Dimensions.get('window').width - 320) / 2) + 5 }}><FAIcon name={I18nManager.isRTL ? 'angle-right' : 'angle-left'} size={30} color="#006d33" /></TouchableOpacity>,
  });

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' ? <View style={{ width: '100%', height: 40, backgroundColor: '#006d33' }} /> : <StatusBar backgroundColor="#006d33" barStyle="dark-content" /> }
        <View style={[styles.row, { paddingLeft: 5, paddingRight: 5 }]}>
          <InfoBar />
        </View>
        <View style={[styles.row, { paddingTop: 10 }]}>
          <View style={styles.buttonsContainer}>
            <FlatList
              data={[
                { key: '1', title: Variables.internalTransfer, navigate: 'TransferList' },
                { key: '2', title: Variables.visa, navigate: '' },
                { key: '3', title: Variables.curruncy, navigate: '' },
                { key: '4', title: Variables.payme, navigate: '' },
                { key: '5', title: Variables.pos, navigate: '' },
                { key: '6', title: Variables.call, navigate: '' },
                { key: '7', title: Variables.share, navigate: '' },
                { key: '8', title: Variables.edit, navigate: '' },
                { key: '9', title: Variables.about, navigate: '' },
              ]}
              renderItem={({ item }) => (
                <SubCategory
                  onPress={() => navigation.navigate(item.navigate)}
                  title={item.title}
                />
              )}
              key={item => item.key}
            />
          </View>
        </View>
        <Footer />
      </View>
    );
  }
}
