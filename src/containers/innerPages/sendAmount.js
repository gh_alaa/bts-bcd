import React, { Component } from 'react';
import {
  I18nManager, Image, StyleSheet, Text, View, TouchableOpacity
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import axios from 'axios';
import { parseString } from 'react-native-xml2js';
import { connect } from 'react-redux';
import { BCurruncyTextInput } from '../../components/TextInputs';
import BButton from '../../components/Buttons';
import serverLink from '../../../configs';

const Variables = I18nManager.isRTL
  ? {
    SendAmount: 'ادفع باستخدام ال SMS',
    enterAmount: 'الرجاء ادخال المبلغ',
    change: 'لتعديل الرقم اضغط',
    here: 'هنا',
    next: 'استمـرار'
  }
  : {
    SendAmount: 'Pay Using SMS',
    enterAmount: 'Please enter the amount',
    change: 'To change the number press',
    here: 'here',
    next: 'Next'
  };


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fee100',
    flexDirection: 'column',
    paddingTop: 20,
    minHeight: '110%'
  },
  title: {
    fontSize: 25,
    color: '#006d33',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.65)',
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Bold' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
    padding: 7,
  },
  subtitle: {
    fontSize: 20,
    color: 'black',
    paddingTop: 15,
    paddingBottom: 15,
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  number: {
    fontSize: 24,
    color: 'black',
    padding: 10,
    width: '85%',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  TextInputContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 8,
    paddingBottom: 8
  },
  rowPress: {
    flexDirection: 'row',
    padding: 0,
    alignItems: 'center',
    width: '85%',
  },
  changeNumberText: {
    fontSize: 18,
    color: 'black',
    padding: 5,
    paddingBottom: 0,
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  Error: {
    fontSize: 12,
    color: 'red',
    paddingTop: 5,
  }
});


class SendAmountScreen extends Component {
  static navigationOptions = { header: null };

  constructor() {
    super();
    this.state = {
      value: 0,
      Amount: 0,
      Currency: '',
      userid: '',
      errorMessage: false,
    };
    this.sendAmount = this.sendAmount.bind(this);
  }

  componentDidMount() {
    const { user } = this.props;
    this.setState({ Currency: user.Currency, userid: user.UserId });
  }

  sendAmount() {
    const that = this;
    const { userid, Amount, Currency } = this.state;
    if (Amount === 0) {
      const errorMessage = I18nManager.isRTL ? 'الرجاء ادخال قيمة صحيحة' : 'Please enter a valid amount';
      this.setState({ errorMessage });
      return;
    }
    const { navigation } = this.props;
    const date = new Date();
    const formattedDate = date.toISOString();
    const xmls = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ConfirmTransactionByUser xmlns="http://tempuri.org/">
      <TransactionType>Sale</TransactionType>
      <TransactionDate>${formattedDate}</TransactionDate>
      <IssuerId>1</IssuerId>
      <IssuerTransactionId>1</IssuerTransactionId>
      <Amount>${Amount}</Amount>
      <Currency>${Currency}</Currency>
      <TerminalId> </TerminalId>
      <MerchantName> </MerchantName>
      <MerchantId> </MerchantId>
      <AccountExpiryDate> </AccountExpiryDate>
      <AdditionalData> </AdditionalData>
      <SenderAccountNumber></SenderAccountNumber>
      <BeneficiaryAccountNumber></BeneficiaryAccountNumber>
      <Sender>${userid}</Sender>
      <Receiver>${navigation.state.params}</Receiver>
      <OTP></OTP>
      <Token></Token>
    </ConfirmTransactionByUser>
  </soap:Body>
</soap:Envelope>
      `;
    axios.post(serverLink, xmls, { headers: { 'Content-Length': '255', 'Content-Type': 'text/xml', SOAPAction: 'http://tempuri.org/ConfirmTransactionByUser' } }).then((res) => {
      parseString(res.data.toString(), (err, result) => {
        const serverData = result['soap:Envelope']['soap:Body'][0].ConfirmTransactionByUserResponse[0].ConfirmTransactionByUserResult[0];
        if (Amount === 0 || Amount === '') {
          navigation.navigate('Success');
        } else if (serverData === 'OTP verification') {
          const Receiver = that.props.navigation.state.params;
          navigation.navigate('SendVerify', [
            Amount, Currency, userid, Receiver
          ]);
        } else {
          that.setState({ errorMessage: serverData.ErrorMessage });
        }
      });
    }).catch((err) => { that.setState({ errorMessage: `SERVER ERROR: ${err}` }); });
  }

  render() {
    const { errorMessage, value, Currency } = this.state;
    const { navigation } = this.props;
    return (
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        enableOnAndroid
        extraScrollHeight={20}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
      >
        <View style={{ padding: 5, paddingBottom: 10 }}>
          <Image
            style={{ height: 89.116, width: 82.032 }}
            source={require('../../../assets/logoOnboard.png')}
          />
        </View>
        <Text style={styles.title}>{Variables.SendAmount}</Text>
        <Text style={[styles.subtitle, { paddingBottom: 0 }]}>{Variables.enterAmount}</Text>
        <View style={styles.rowPress}>
          <Text style={styles.changeNumberText}>{Variables.change}</Text>
          <TouchableOpacity onPress={() => { navigation.navigate('SendPhone'); }}>
            <Text style={[styles.changeNumberText, { color: '#006d33' }]}>{Variables.here}</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.number}>{navigation.state.params}</Text>
        {errorMessage && <Text style={styles.Error}>{errorMessage}</Text>}
        <View style={styles.TextInputContainer}>
          <BCurruncyTextInput currency={Currency} value={value} keyboard="numeric" onChange={text => this.setState({ value: text, Amount: text.replace(`${Currency} `, '') })} />
        </View>
        <BButton buttonText={Variables.next} onPress={this.sendAmount} />
      </KeyboardAwareScrollView>
    );
  }
}


const mapStateToProps = state => ({
  user: state.user,
});


export default connect(mapStateToProps)(SendAmountScreen);
