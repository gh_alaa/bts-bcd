import React, { Component } from 'react';
import {
  Text, FlatList, TouchableOpacity, I18nManager, StyleSheet, View, Platform, StatusBar
} from 'react-native';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';
import { parseString } from 'react-native-xml2js';
import { connect } from 'react-redux';
import SubCategory from '../../components/subCategory';
import Footer from '../../components/Footer';
import Loading from '../../components/loading';
import serverLink from '../../../configs';
import getXmlData from '../../../helper';

const Variables = I18nManager.isRTL
  ? {
    noData: 'لا يوجد معلومات',
    Transactions: 'الحركات'
  }
  : {
    noData: 'No Data',
    Transactions: 'Transactions'
  };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgba(0,147,75,0.02)',
    flexDirection: 'column',
    paddingTop: 20,
    paddingBottom: 80,
  },
  buttonsContainer: {
    width: '95%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column',
  },
  filterContainer: {
    position: 'absolute',
    width: '100%',
    height: 40,
    backgroundColor: '#fee100',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  filterText: {
    color: 'black',
    fontFamily: 'Arial',
    fontWeight: '500',
    padding: 10,
  }
});


type Props = {};
class TransferList extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    title: 'Home',
    headerStyle: {
      backgroundColor: '#fee100',
    },
    headerTitle: (
      <View style={{
        padding: 5, width: '100%', alignItems: 'center', justifyContent: 'center'
      }}
      >
        <Text style={{
          fontSize: 18, color: 'black', fontWeight: '500', fontFamily: 'Arial'
        }}
        >
          {Variables.Transactions}
        </Text>
      </View>),
    headerTintColor: '#fff',
    headerTitleStyle: {
      textAlign: 'center',
      width: '100%',
      display: 'flex',
    },
    headerRight: (
      <TouchableOpacity
        onPress={() => navigation.state.params.handleFilter()}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          paddingRight: 20,
          paddingLeft: 20
        }}
      >
        <FAIcon name="filter" size={21} color="#006d33" />
      </TouchableOpacity>),
    headerLeft: (
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          paddingRight: 20,
          paddingLeft: 20
        }}
      >
        <FAIcon name={I18nManager.isRTL ? 'angle-right' : 'angle-left'} size={30} color="#006d33" />
      </TouchableOpacity>),
  });

  constructor() {
    super();
    this.state = {
      vwTransactions: false,
      refresh: false,
      ready: false,
      filter: false,
      fromDate: 'From Date',
      toDate: 'To Date',
    };
    this.getList = this.getList.bind(this);
    this.toggleFilter = this.toggleFilter.bind(this);
  }

  componentDidMount() {
    const { navigation, user } = this.props;
    this.setState({ userid: user.UserId });
    this.getList(user.UserId);
    navigation.setParams({
      handleFilter: this.toggleFilter
    });
  }

  getList(userid) {
    const that = this;
    const xmls = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetUservwTransactions xmlns="http://tempuri.org/">
      <UserId>${userid}</UserId>
      <Filter1></Filter1>
      <Filter2></Filter2>
    </GetUservwTransactions>
  </soap:Body>
</soap:Envelope>`;
    axios.post(serverLink, xmls, { headers: { 'Content-Length': '255', 'Content-Type': 'text/xml', SOAPAction: 'http://tempuri.org/GetUservwTransactions' } }).then((res) => {
      parseString(res.data.toString(), (err, result) => {
        const serverData = getXmlData(result, 'GetUservwTransactions');
        if (serverData.ErrorMessage === '') {
          that.setState({ vwTransactions: serverData.vwTransactions });
          that.setState({ refresh: false, ready: true });
        } else {
          that.setState({ errorMessage: serverData.ErrorMessage });
        }
      });
    }).catch(() => {
      that.setState({ errorMessage: Variables.errorMessage });
    });
  }

  toggleFilter() {
    const { filter } = this.state;
    this.setState({ filter: !filter });
  }

  render() {
    const { navigation } = this.props;
    const {
      ready, refresh, vwTransactions, userid, filter, fromDate, toDate
    } = this.state;
    const DATA = vwTransactions
      ? Object.assign([], vwTransactions).reverse()
      : [];
    if (ready) {
      return (
        <View style={[styles.container, filter && { paddingTop: 55 }]}>
          {filter && (
          <View style={styles.filterContainer}>
            <TouchableOpacity><Text style={styles.filterText}>{fromDate}</Text></TouchableOpacity>
            <Text style={styles.filterText}>-</Text>
            <TouchableOpacity><Text style={styles.filterText}>{toDate}</Text></TouchableOpacity>
          </View>
          )}
          {Platform.OS === 'ios' ? <View style={{ width: '100%', height: 40, backgroundColor: '#fee100' }} /> : <StatusBar backgroundColor="#fee100" barStyle="dark-content" /> }
          <View style={styles.buttonsContainer}>
            <FlatList
              ListEmptyComponent={<View style={{ alignItems: 'center', justifyContent: 'center' }}><Text>{Variables.noData}</Text></View>}
              progressViewOffset={10}
              refreshing={refresh}
              onRefresh={() => { this.setState({ refresh: true }); this.getList(userid); }}
              data={DATA}
              renderItem={({ item }) => <SubCategory onPress={() => navigation.navigate('TransactionDetails', item)} subtitle={item.TxnSubTitle} title={item.TxnTitle} />}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <Footer />
        </View>
      );
    }
    return (<Loading />);
  }
}


const mapStateToProps = state => ({
  user: state.user,
});


export default connect(mapStateToProps)(TransferList);
