/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  Text, FlatList, TouchableOpacity, I18nManager, StyleSheet, View, Platform, StatusBar
} from 'react-native';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import Footer from '../../components/Footer';


const Variables = I18nManager.isRTL
  ? {
    Name: 'محمد أحمد علي عبدالله',
    active: 'آخر نشاط للحساب',
    SaleTransaction: 'صفقة بيع',
    NormalTransaction: 'صفقة عادية',
    date: 'التاريخ:',
    status: 'الحالة:'
  }
  : {
    Name: 'Mohammad Ahmad Ali Abdullah',
    active: 'Last Activity',
    SaleTransaction: 'Sale-Transaction',
    NormalTransaction: 'Normal-Transaction',
    date: 'Date:',
    status: 'Status:'
  };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgba(0,147,75,0.02)',
    flexDirection: 'column',
    paddingBottom: 120,
  },
  row: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    width: '95%',
    flex: 1,
  },
  buttonsContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column',
  }
});


type Props = {};
export default class App extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    title: 'Home',
    headerStyle: {
      backgroundColor: '#fee100',
      shadowOpacity: 0,
      shadowOffset: {
        height: 0,
      },
      shadowRadius: 0,
      elevation: 0,
    },
    headerTitle: (
      <View style={{
        padding: 5, width: '100%', alignItems: 'center', justifyContent: 'center'
      }}
      >
        <Text style={{
          fontSize: 18, color: 'black', fontWeight: '500', fontFamily: 'Arial'
        }}
        >
          {navigation.state.params['Transaction Id']}
        </Text>
      </View>),
    headerTintColor: '#fff',
    headerTitleStyle: {
      textAlign: 'center',
      width: '100%',
      display: 'flex',
    },
    headerRight: <View />,
    headerLeft: <TouchableOpacity onPress={() => navigation.goBack()} style={{ paddingRight: 20, paddingLeft: 20 }}><FAIcon name={I18nManager.isRTL ? 'angle-right' : 'angle-left'} size={30} color="#006d33" /></TouchableOpacity>,
  });

  render() {
    const { navigation } = this.props;
    const { params } = navigation.state;
    const aditionalInfo = { ...params };
    delete aditionalInfo.MerchantName;
    delete aditionalInfo.TransactionType;
    delete aditionalInfo.Amount;
    delete aditionalInfo['Transaction Date'];
    delete aditionalInfo.Status;
    delete aditionalInfo['Transaction Id'];
    delete aditionalInfo.TxnSubTitle;
    delete aditionalInfo.TxnTitle;

    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' ? <View style={{ width: '100%', height: 40, backgroundColor: '#fee100' }} /> : <StatusBar backgroundColor="#fee100" barStyle="dark-content" /> }
        <View style={{ width: '100%', backgroundColor: '#fee100', padding: 10 }}>
          <View style={{
            backgroundColor: 'white', width: '100%', borderRadius: 5, padding: 20, elevation: 3
          }}
          >
            <Text style={{
              fontWeight: '900', fontSize: 20, color: 'black', fontFamily: 'Arial', width: '100%'
            }}
            >
              {params.TxnTitle}
            </Text>
            <Text style={{
              fontSize: 15, color: 'grey', fontFamily: 'Arial', width: '100%', borderBottomWidth: 1, paddingTop: 5, paddingBottom: 10, borderColor: 'grey'
            }}
            >
              {params.TransactionType}
            </Text>
            <Text style={{
              padding: 20, fontWeight: '500', textAlign: 'center', fontSize: 30, color: 'black', fontFamily: 'Arial', width: '100%', borderBottomWidth: 1, borderColor: 'grey'
            }}
            >
              {params.Amount}
            </Text>
            <View style={{
              paddingTop: 10, width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'
            }}
            >
              <Text style={{
                fontWeight: 'bold', fontSize: 15, color: 'grey', fontFamily: 'Arial',
              }}
              >
                {Variables.date}
              </Text>
              <Text style={{ fontSize: 15, color: 'grey', fontFamily: 'Arial', }}>{params['Transaction Date']}</Text>
            </View>
            <View style={{
              paddingTop: 10, width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'
            }}
            >
              <Text style={{
                fontWeight: 'bold', fontSize: 15, color: 'grey', fontFamily: 'Arial',
              }}
              >
                {Variables.status}
              </Text>
              <Text style={[params.Status === 'Confirmed' && { color: 'green' }, { fontSize: 15, fontFamily: 'Arial', }]}>{params.Status}</Text>
            </View>
          </View>
        </View>
        <Text style={{
          width: '100%', textAlign: 'left', color: 'black', padding: 10, paddingTop: 20, fontSize: 20
        }}
        >
Other Details:
        </Text>
        <View style={[styles.row, {
          padding: 5, borderRadius: 4, borderWidth: 1, borderColor: 'grey'
        }]}
        >
          <FlatList
            data={Object.keys(aditionalInfo)}
            renderItem={({ item, index }) => {
              console.log(item); // eslint-disable-line
              return (
                <View style={{
                  width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderBottomWidth: 1, padding: 5
                }}
                >
                  <Text>{Object.keys(aditionalInfo)[index]}</Text>
                  <Text>{aditionalInfo[Object.keys(aditionalInfo)[index]]}</Text>
                </View>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <Footer />
      </View>
    );
  }
}
