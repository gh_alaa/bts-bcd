import React, { Component } from 'react';
import {
  I18nManager, Image, StyleSheet, Text, View, StatusBar, Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BTextInput } from '../../components/TextInputs';
import BButton from '../../components/Buttons';


const Variables = I18nManager.isRTL
  ? {
    ReceiveAmount: 'ادفعلي باستخدام ال SMS',
    enterPhone: ' يرجى إدخال هاتف المرسل',
    next: 'استمـرار'
  }
  : {
    ReceiveAmount: 'Edfali using SMS',
    enterPhone: 'Please enter the sender phone number',
    next: 'Next'
  };


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fee100',
    flexDirection: 'column',
    paddingTop: 20,
    minHeight: '110%'
  },
  title: {
    fontSize: 25,
    color: '#006d33',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.65)',
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Bold' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
    padding: 7,
  },
  subtitle: {
    fontSize: 20,
    color: 'black',
    padding: 15,
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  number: {
    fontSize: 24,
    color: 'black',
    padding: 10,
    width: '85%',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  TextInputContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 8,
    paddingBottom: 8
  }
});


class PhoneScreen extends Component {
  static navigationOptions = { header: null };

  constructor() {
    super();
    this.state = {
      phone: ''
    };
  }

  render() {
    const { navigation } = this.props;
    const { phone } = this.state;
    return (
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        enableOnAndroid
        extraScrollHeight={20}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
      >
        {Platform.OS === 'ios' ? <View style={{ width: '100%', height: 40, backgroundColor: '#fee100' }} /> : <StatusBar backgroundColor="#fee100" barStyle="dark-content" /> }
        <View style={{ padding: 5, paddingBottom: 10 }}>
          <Image
            style={{ height: 89.116, width: 82.032 }}
            source={require('../../../assets/logoOnboard.png')}
          />
        </View>
        <Text style={styles.title}>{Variables.ReceiveAmount}</Text>
        <Text style={styles.subtitle}>{Variables.enterPhone}</Text>
        <View style={styles.TextInputContainer}>
          <BTextInput keyboard="phone-pad" onChange={text => this.setState({ phone: text })} />
        </View>
        <BButton buttonText={Variables.next} onPress={() => { navigation.navigate('ReceiveAmount', phone); }} />
      </KeyboardAwareScrollView>
    );
  }
}


export default PhoneScreen;
