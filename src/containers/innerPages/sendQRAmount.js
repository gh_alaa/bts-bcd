import React, { Component } from 'react';
import {
  I18nManager, Image, StyleSheet, Text, View, StatusBar, Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BCurruncyTextInput } from '../../components/TextInputs';
import BButton from '../../components/Buttons';


const Variables = I18nManager.isRTL
  ? {
    SendAmount: 'ادفع باستخدام ال QR',
    enterAmount: 'الرجاء ادخال المبلغ',
    change: 'لتعديل الرقم اضغط',
    here: 'هنا',
    next: 'استمـرار'
  }
  : {
    SendAmount: 'Pay Using QR',
    enterAmount: 'Please enter the amount',
    change: 'To change the number press',
    here: 'here',
    next: 'Next'
  };


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fee100',
    flexDirection: 'column',
    paddingTop: 20,
    minHeight: '110%'
  },
  title: {
    fontSize: 25,
    color: '#006d33',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.65)',
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Bold' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
    padding: 7,
  },
  subtitle: {
    fontSize: 20,
    color: 'black',
    paddingTop: 15,
    paddingBottom: 15,
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  number: {
    fontSize: 24,
    color: 'black',
    padding: 10,
    width: '85%',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  TextInputContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 8,
    paddingBottom: 8
  },
  rowPress: {
    flexDirection: 'row',
    padding: 0,
    alignItems: 'center',
    width: '85%',
  },
  changeNumberText: {
    fontSize: 18,
    color: 'black',
    padding: 5,
    paddingBottom: 0,
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  }
});


class PhoneScreen extends Component {
  static navigationOptions = { header: null };

  constructor() {
    super();
    this.state = {
      Amount: 0,
      value: 0,
    };
  }

  componentDidMount() {

  }

  render() {
    const { navigation } = this.props;
    const { Amount, value } = this.state;
    const getData = { ...navigation.state.params.data };
    getData.Amount = Amount;
    return (
      <KeyboardAwareScrollView
        enableOnAndroid
        extraScrollHeight={20}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
      >
        {Platform.OS === 'ios' ? <View style={{ width: '100%', height: 40, backgroundColor: '#fee100' }} /> : <StatusBar backgroundColor="#fee100" barStyle="dark-content" /> }
        <View style={{ padding: 5, paddingBottom: 10 }}>
          <Image
            style={{ height: 89.116, width: 82.032 }}
            source={require('../../../assets/logoOnboard.png')}
          />
        </View>
        <Text style={styles.title}>{Variables.SendAmount}</Text>
        <Text style={[styles.subtitle, { paddingBottom: 0 }]}>{Variables.enterAmount}</Text>
        <View style={styles.TextInputContainer}>
          <BCurruncyTextInput currency={getData.Currency} value={value} keyboard="numeric" onChange={text => this.setState({ value: text, Amount: text.replace(`${getData.Currency} `, '') })} />
        </View>
        <BButton
          buttonText={Variables.next}
          onPress={() => {
            navigation.navigate('SendQRVerify', { data: getData });
          }}
        />
      </KeyboardAwareScrollView>
    );
  }
}


export default PhoneScreen;
