import React, { Component } from 'react';
import {
  I18nManager, Image, StyleSheet, Text, View, StatusBar, Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import axios from 'axios';
import { parseString } from 'react-native-xml2js';
import { BSecureTextInput } from '../../components/TextInputs';
import BButton from '../../components/Buttons';
import serverLink from '../../../configs';

const Variables = I18nManager.isRTL
  ? {
    SecureAccess: 'تأمين الدخول',
    confirm: 'لتأكيد رقم هاتفك',
    confirmationMsg: 'أدخل الرمز المكون من ٤ آرقام والذي آرسلناه للرقم',
    confirmIt: 'استمـرار'
  }
  : {
    SecureAccess: 'Secure Access',
    confirm: 'Confirm mobile number',
    confirmationMsg: 'Enter the 4-digit code we sent to',
    confirmIt: 'Confirm'
  };


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fee100',
    flexDirection: 'column',
    paddingTop: 20,
    minHeight: '110%'
  },
  title: {
    fontSize: 25,
    color: '#006d33',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.65)',
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Bold' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
    padding: 7,
  },
  subtitle: {
    fontSize: 20,
    color: 'black',
    padding: 15,
    width: '85%',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  number: {
    fontSize: 24,
    color: 'black',
    padding: 10,
    width: '85%',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  TextInputContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 8,
    paddingBottom: 8
  },
  Error: {
    fontSize: 12,
    color: 'red',
    padding: 5,
  }
});


class PhoneScreen extends Component {
  static navigationOptions = { header: null };

  constructor() {
    super();
    this.state = {
      code: ''
    };
    this.sendAmount = this.sendAmount.bind(this);
  }

  sendAmount() {
    const that = this;
    const { code } = this.state;
    const { navigation } = this.props;
    const date = new Date();
    const formattedDate = date.toISOString();
    const xmls = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ConfirmTransactionByUser xmlns="http://tempuri.org/">
      <TransactionType>Sale</TransactionType>
      <TransactionDate>${formattedDate}</TransactionDate>
      <IssuerId>1</IssuerId>
      <IssuerTransactionId>1</IssuerTransactionId>
      <Amount>${navigation.state.params[0]}</Amount>
      <Currency>${navigation.state.params[1]}</Currency>
      <TerminalId> </TerminalId>
      <MerchantName> </MerchantName>
      <MerchantId> </MerchantId>
      <AccountExpiryDate> </AccountExpiryDate>
      <AdditionalData> </AdditionalData>
      <SenderAccountNumber></SenderAccountNumber>
      <BeneficiaryAccountNumber></BeneficiaryAccountNumber>
      <Sender>${navigation.state.params[2]}</Sender>
      <Receiver>${navigation.state.params[3]}</Receiver>
      <OTP>${code}</OTP>
      <Token></Token>
    </ConfirmTransactionByUser>
  </soap:Body>
</soap:Envelope>
      `;
    axios.post(serverLink, xmls, { headers: { 'Content-Length': '255', 'Content-Type': 'text/xml', SOAPAction: 'http://tempuri.org/ConfirmTransactionByUser' } }).then((res) => {
      parseString(res.data.toString(), (err, result) => {
        const serverData = result['soap:Envelope']['soap:Body'][0].ConfirmTransactionByUserResponse[0].ConfirmTransactionByUserResult[0];
        if (serverData === 'Confirmed') {
          navigation.navigate('Success');
        } else {
          that.setState({ errorMessage: serverData });
        }
      });
    }).catch((err) => { console.error(err); /* eslint-disable-line */});
  }

  render() {
    const { errorMessage } = this.state;
    const { navigation } = this.props;
    return (
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        enableOnAndroid
        extraScrollHeight={20}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
      >
        {Platform.OS === 'ios' ? <View style={{ width: '100%', height: 40, backgroundColor: '#fee100' }} /> : <StatusBar backgroundColor="#fee100" barStyle="dark-content" /> }
        <View style={{ padding: 5, paddingBottom: 10 }}>
          <Image
            style={{ height: 89.116, width: 82.032 }}
            source={require('../../../assets/logoOnboard.png')}
          />
        </View>
        <Text style={styles.title}>{Variables.SecureAccess}</Text>
        <Text style={[styles.subtitle, { paddingBottom: 0 }]}>{Variables.confirm}</Text>
        <Text style={[styles.subtitle, { padding: 0 }]}>{Variables.confirmationMsg}</Text>
        <Text style={styles.number}>{navigation.state.params[2]}</Text>
        {errorMessage && <Text style={styles.Error}>{errorMessage}</Text>}

        <BSecureTextInput keyboard="numeric" onChange={text => this.setState({ code: text })} />
        <BButton buttonText={Variables.confirmIt} onPress={this.sendAmount} />
      </KeyboardAwareScrollView>
    );
  }
}


export default PhoneScreen;
