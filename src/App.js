/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { createStackNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';
import firebase from 'react-native-firebase';
import { View } from 'react-native';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import VerifyScreen from './containers/onBoarding/verify';
import PhoneScreen from './containers/onBoarding/phone';
import LoginScreen from './containers/onBoarding/login';
import HomeScreen from './containers/mainPages/home';
import OtherServicesScreen from './containers/innerPages/otherServices';
import PayMeScreen from './containers/innerPages/payme';
import TransferScreen from './containers/innerPages/transfer';
import TransferListScreen from './containers/innerPages/transferList';
import TransferDetailsScreen from './containers/innerPages/transactionDetails';
import SendAmountScreen from './containers/innerPages/sendAmount';
import SendPhoneScreen from './containers/innerPages/sendPhone';
import SendVerifyScreen from './containers/innerPages/sendVerify';
import ReceiveAmountScreen from './containers/innerPages/receiveAmount';
import ReceivePhoneScreen from './containers/innerPages/receivePhone';
import ReceiveVerifyScreen from './containers/innerPages/receiveVerify';
import SendQRAmountScreen from './containers/innerPages/sendQRAmount';
import SendQRCameraScreen from './containers/innerPages/sendQRCamera';
import SendQRVerifyScreen from './containers/innerPages/sendQRVerify';
import ReceiveQRScreen from './containers/innerPages/receiveQR';
import ReceiveQRCodeScreen from './containers/innerPages/receiveQRCode';
import SuccessScreen from './containers/innerPages/success';
import AuthLoadingScreen from './components/splash';
import { ModalSuccess, Modal } from './components/modal';
import userReducer from './reducers';

const store = createStore(userReducer);

const VerifyStack = createStackNavigator({
  Phone: { screen: PhoneScreen },
  Verify: { screen: VerifyScreen },
});

const AuthStack = createStackNavigator({
  Login: { screen: LoginScreen },
});

const AppStack = createStackNavigator({
  Home: { screen: HomeScreen },
  OtherServices: { screen: OtherServicesScreen },
  PayMe: { screen: PayMeScreen },
  Transfer: { screen: TransferScreen },
  TransferList: { screen: TransferListScreen },
  TransactionDetails: { screen: TransferDetailsScreen },
  SendPhone: { screen: SendPhoneScreen },
  SendAmount: { screen: SendAmountScreen },
  SendVerify: { screen: SendVerifyScreen },
  ReceivePhone: { screen: ReceivePhoneScreen },
  ReceiveAmount: { screen: ReceiveAmountScreen },
  ReceiveVerify: { screen: ReceiveVerifyScreen },
  SendQRAmount: { screen: SendQRAmountScreen },
  SendQRCamera: { screen: SendQRCameraScreen },
  SendQRVerify: { screen: SendQRVerifyScreen },
  ReceiveQR: { screen: ReceiveQRScreen },
  ReceiveQRCode: { screen: ReceiveQRCodeScreen },
  Success: { screen: SuccessScreen },
});


const AppContainer = createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Verify: VerifyStack,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));

firebase.messaging().hasPermission()
  .then((enabled) => {
    if (enabled) {
      // user has permissions
      console.log('enabled');
    } else {
      console.log('not enabled');
      // user doesn't have permission
      try {
        firebase.messaging().requestPermission();
        console.log('asked');
        // User has authorised
      } catch (error) {
        console.log(error);
        // User has rejected permissions
      }
    }
  });

firebase.messaging().getToken()
  .then((fcmToken) => {
    if (fcmToken) {
      // user has a device token
      console.log(fcmToken);
    } else {
      // user doesn't have a device token yet
      console.log('no token');
    }
  });


class Main extends Component {
  constructor() {
    super();
    this.state = {
      notification: false,
      type: ''
    };
  }

  componentDidMount() {
    // get notification when app is in foreground
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      // Process your notification as required
      console.log('app foreground:', notification.data);
      const { data } = notification;
      this.setState({
        notification: data.show, type: data.type, title: data.title, body: data.body
      });
    });
    // get notification when it's opened and the app was fully closed before
    firebase.notifications().getInitialNotification()
      .then((notificationOpen) => {
        console.log('there');
        if (notificationOpen) {
          // App was opened by a notification
          // Get the action triggered by the notification being opened
          const { action } = notificationOpen;
          // Get information about the notification that was opened
          const { notification } = notificationOpen;
          console.log('opened when app closed:', notification.data);
          const { data } = notification;
          this.setState({
            notification: data.show, type: data.type, title: data.title, body: data.body
          });
          // firebase.notifications().removeDeliveredNotification(notification.notificationId);
        }
      });
  }

  componentWillUnmount() {
    this.notificationListener();
  }

  render() {
    console.log(this.state);
    const {
      type, notification, body, title
    } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Provider store={store}>
          <AppContainer />
        </Provider>
        {notification && (type === 'success' ? <ModalSuccess title={title} onPress={() => this.setState({ notification: false, type: '' })} /> : <Modal title={title} body={body} onPress={() => this.setState({ notification: false, type: '' })} />)}
      </View>
    );
  }
}

export default Main;
