import React from 'react';
import LottieView from 'lottie-react-native';

const Loading = (props) => {
  const { width } = props;
  return (
    <LottieView
      source={require('../../assets/animations/confirm.json')}
      autoPlay
      loop={false}
      width={width || 200}
    />
  );
};

export default Loading;
