import React from 'react';
import {
  Linking, I18nManager, View, Text, StyleSheet,TouchableOpacity
} from 'react-native';
import FAIcon from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({
  footer: {
    height: 50,
    width: '100%',
    backgroundColor: '#fee100',
    bottom: 0,
    position: 'absolute'

  },
  iconsBar: {
    width: '100%',
    alignItems:'center',
    justifyContent: 'center',
    position: 'absolute',
    top: -15,
    flexDirection: 'row',
  }
});

const Footer = (props) => {
  const { Variables } = props;
  return (
    <View style={styles.footer}>
      <View style={styles.iconsBar}>
        <TouchableOpacity onPress={()=>Linking.openURL('https://www.instagram.com/bank_bcd.ly/')} style={{alignItems:'center',justifyContent:'center', borderRadius: 2, marginLeft: 5, marginRight: 5, width: 30,height: 30, backgroundColor: '#006d33'}}>
          <FAIcon name="instagram" size={20} color="white" />
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>Linking.openURL('https://bcd.ly')} style={{alignItems:'center',justifyContent:'center', borderRadius: 2, marginLeft: 5, marginRight: 5, width: 30,height: 30, backgroundColor: '#006d33'}}>
          <FAIcon name="globe" size={20} color="white" />
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>Linking.openURL('mailto:info@bcd.ly')} style={{alignItems:'center',justifyContent:'center', borderRadius: 2, marginLeft: 5, marginRight: 5, width: 30,height: 30, backgroundColor: '#006d33'}}>
          <FAIcon name="envelope-o" size={20} color="white" />
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>Linking.openURL('https://www.facebook.com/bcd.ly/')} style={{alignItems:'center',justifyContent:'center', borderRadius: 2, marginLeft: 5, marginRight: 5, width: 30,height: 30, backgroundColor: '#006d33'}}>
          <FAIcon name="facebook" size={20} color="white" />
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>Linking.openURL('https://bcd.ly')} style={{alignItems:'center',justifyContent:'center', borderRadius: 2, marginLeft: 5, marginRight: 5, width: 30,height: 30, backgroundColor: '#006d33'}}>
          <FAIcon name="home" size={20} color="white" />
        </TouchableOpacity>
      </View>
    </View>
  );
};


export default Footer;
