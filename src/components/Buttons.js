import React,{Component} from 'react';
import {
  View,I18nManager,Button, TouchableHighlight, Text, TouchableOpacity, StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  btnText: {
    fontSize: 20,
    color: 'white',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Bold' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  bcdBtnLg: {
    marginTop: 10,
    width: '85%',
    backgroundColor: '#006d33',
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  shadow: {
    position: 'absolute',
    height: 2,
    width: '100%',
    alignSelf: 'center',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 1,
    shadowOpacity: 1,
    zIndex: 1,
  }
});

class BButton extends Component {
  constructor() {
    super();
    this.state = {
      color: true
    }
  }
  render() {
    const { onPress, buttonText,style } = this.props;
    return (
      <View style={[style,{width: '100%', alignItems: 'center', justifyContent: 'center'}]}>
        <TouchableHighlight onHideUnderlay={()=>this.setState({color:true})} onShowUnderlay={()=>this.setState({color:false})} underlayColor='#D0D0D0' onPress={onPress} style={styles.bcdBtnLg}>
          <View style={{height: '100%',width: '100%', alignItems: 'center',justifyContent:'center'}}>
            <View style={[styles.shadow,{top: 0,height: '95%', width: 1,right: 1, backgroundColor: 'rgba(0,0,0,0.3)',shadowColor: 'rgba(0,0,0,0.3)'}]}></View>
            <View style={[styles.shadow,{bottom: 0, backgroundColor: 'rgba(0,0,0,0.3)',shadowColor: 'rgba(0,0,0,0.3)'}]}></View>
            <View style={[styles.shadow,{top: 0, backgroundColor: 'rgba(255,255,255,0.3)',shadowColor: 'rgba(255,255,255,0.3)'}]}></View>
            <View style={[styles.shadow,{top: 0,height: '95%', width: 1,left: 1, backgroundColor: 'rgba(255,255,255,0.3)',shadowColor: 'rgba(255,255,255,0.3)'}]}></View>
            <Text style={[styles.btnText,this.state.color ? {color: 'white'} : {color: '#006d33'} ]}>{buttonText}</Text>
          </View>
        </TouchableHighlight>
      </View>
    )
  }
}



export default BButton;
