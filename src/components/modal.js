
import React from 'react';
import {
  TouchableOpacity, Text, View, StyleSheet
} from 'react-native';
import BButton from './Buttons';
import Confirm from './confirm';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    zIndex: 999,
    top: 0,
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  modal: {
    width: 270,
    height: 320,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'white',
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  lottieContainer: {
    width: 270,
    height: 270,
  }
});


const Modal = (props) => {
  const { onPress, title, body } = props;
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress} style={styles.modal}>
        <Text style={{ fontSize: 30, color: 'black', fontFamily: 'Arial' }}>{title}</Text>
        <Text style={{
          fontSize: 20, width: 200, color: 'black', fontFamily: 'Arial'
        }}
        >
          {body}
        </Text>
        <BButton buttonText="Dismiss" onPress={onPress} style={{ position: 'absolute', bottom: 10 }} />
      </TouchableOpacity>
    </View>
  );
};

const ModalSuccess = (props) => {
  const { onPress, title } = props;
  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <View style={{
          width: '100%',
          height: 20,
          alignItems: 'center',
          justifyContent: 'center',
          paddingTop: 10,
        }}
        >
          <Text style={{ fontSize: 15, color: 'black', fontFamily: 'Arial' }}>{title}</Text>
        </View>
        <View style={styles.lottieContainer}>
          <Confirm width={270} />
        </View>
        <BButton buttonText="Dismiss" onPress={onPress} style={{ position: 'absolute', bottom: 10 }} />
      </View>
    </View>
  );
};

export { ModalSuccess, Modal };
