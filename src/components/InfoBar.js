import React, { Component } from 'react';
import {
  TouchableOpacity, I18nManager, View, Text, StyleSheet
} from 'react-native';
import { connect } from 'react-redux';

const styles = StyleSheet.create({
  infoBar: {
    height: 50,
    width: '100%',
    backgroundColor: '#D0D0D0',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.65)',
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 3, width: 0 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 2, // IOS
    elevation: 2, // Android
    flexDirection: 'column',
  },
  smallText: {
    fontSize: 15,
    paddingLeft: 5,
    paddingRight: 5,
    color: 'black',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
});

const Variables = I18nManager.isRTL
  ? {
    accountid: 'حساب',
    balance: 'رصيد حساب',
  }
  : {
    accountid: 'Account',
    balance: 'Balance',
  };


class infoBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: ''
    };
  }

  componentWillMount() {
    const { userInfo, accounts } = this.props;
    this.setState({ fullName: userInfo['Full Name'], accounts });
  }

  render() {
    const { fullName, accounts } = this.state;
    return (
      <TouchableOpacity onPress={() => console.log(this.props)} style={styles.infoBar}>
        <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={styles.smallText}>{fullName}</Text>
        </View>
        <View style={{
          width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
        }}
        >
          <Text style={styles.smallText}>{accounts[0] ? `${Variables.accountid}: ${accounts[0].Pan}` : '-'}</Text>
          <Text style={styles.smallText}>{accounts[0] ? `${Variables.balance}: ${accounts[0].Balance}` : '-'}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = state => ({
  userInfo: state.userInfo,
  accounts: state.accounts,
  user: state.user,
});


export default connect(mapStateToProps)(infoBar);
