import React from 'react';
import {
  Image, AsyncStorage, View
} from 'react-native';
import { connect } from 'react-redux';

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  bootstrapAsync = async () => {
    const { navigation, user } = this.props;
    const verified = await AsyncStorage.getItem('@BCD:verified');
    const loggedIn = user;
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    const afterVerify = loggedIn ? 'App' : 'Auth';
    navigation.navigate(verified ? afterVerify : 'Verify');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={{
        width: '100%', height: '100%', position: 'absolute', zIndex: 100, backgroundColor: '#fee100', flex: 1, alignItems: 'center', justifyContent: 'center'
      }}
      >
        <Image
          style={{ height: 133.674, width: 123.048, marginBottom: 30 }}
          source={require('../../assets/logoOnboard.png')} // eslint-disable-line global-require
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});


export default connect(mapStateToProps)(AuthLoadingScreen);
