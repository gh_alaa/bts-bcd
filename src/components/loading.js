import React from 'react';
import LottieView from 'lottie-react-native';

const Loading = () => (
  <LottieView
    source={require('../../assets/animations/loading.json')}
    autoPlay
    loop
    width="100%"
  />
);

export default Loading;
