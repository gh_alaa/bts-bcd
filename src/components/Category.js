import React,{Component} from 'react';
import {
  Dimensions,I18nManager, Image, View, Text, TouchableHighlight, StyleSheet
} from 'react-native';


const maxSize = 100;
const finalSize = (Dimensions.get('window').height-320)/4;
const sizes = (finalSize < maxSize) ? finalSize : maxSize;

const styles = StyleSheet.create({
  Item: {
    width: sizes+1,
    height: sizes+1,
    backgroundColor: '#fee100',
    borderRadius: 11,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, // IOS
    elevation: 3, // Android
    flexDirection: 'row',
  },
  titleText: {
    fontSize: I18nManager.isRTL ? 14 : 12,
    color: 'black',
    width: 100,
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  ItemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
    elevation: 10,
  },
  ItemContainer2: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
    elevation: 3,
  },
  Item2: {
    width: '100%',
    height: 50,
    backgroundColor: '#fee100',
    borderRadius: 11,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, // IOS
    elevation: 3, // Android
    flexDirection: 'row',
  },
});

class Category extends Component {
  constructor() {
    super();
    this.state = {
      color: true
    }
  }
  render() {
    const { icon,title,onPress } = this.props;
    return (
      <TouchableHighlight underlayColor='transparent' onHideUnderlay={()=>this.setState({color:true})} onShowUnderlay={()=>this.setState({color:false})} onPress={onPress} style={styles.ItemContainer}>
        <View style={{alignItems: 'center',justifyContent: 'center'}}>
          <View style={[styles.Item,this.state.color ? {backgroundColor: '#fee100'} : {backgroundColor: '#D0D0D0'}]}>
            <View style={{
                shadowColor: 'rgba(255,255,255, .4)', // IOS
                shadowOffset: { height: 1, width: 1 }, // IOS
                shadowOpacity: 1, // IOS
                shadowRadius: 1, // IOS
                elevation: 1, // Android
                position: 'absolute', width: sizes, height: sizes,borderRadius: 8, borderColor: 'rgba(255,255,255,0.6)', borderTopWidth: 2,borderLeftWidth: 2}}></View>
              {this.state.color ? <Image
                  style={{width: sizes, height: sizes,position: 'absolute'}}
                  source={require('../../assets/iconHolder.png')}
                /> :
                <Image
                  style={{width: sizes, height: sizes,position: 'absolute'}}
                  source={require('../../assets/iconHolder2.png')}
                />}
              {icon}
            </View>
            <Text style={[styles.titleText,this.state.color ? {color: 'black'} : {color: '#006d33'}]}>{title}</Text>
        </View>
      </TouchableHighlight>
    );
  };
}

class Category2 extends Component {
  constructor() {
    super();
    this.state = {
      color: true
    }
  }
  render() {
    const { icon,title,onPress } = this.props;
    return (
      <TouchableHighlight underlayColor='transparent' onHideUnderlay={()=>this.setState({color:true})} onShowUnderlay={()=>this.setState({color:false})} onPress={onPress} style={styles.ItemContainer2}>
        <View style={[styles.Item2,this.state.color ? {backgroundColor: '#fee100'} : {backgroundColor: '#D0D0D0'}]}>
          <View style={{
              shadowColor: 'rgba(255,255,255, .4)', // IOS
              shadowOffset: { height: 1, width: 1 }, // IOS
              shadowOpacity: 1, // IOS
              shadowRadius: 1, // IOS
              elevation: 1, // Android
              position: 'absolute', width: '100%', height: 50,borderRadius: 8, borderColor: 'rgba(255,255,255,0.6)', borderTopWidth: 2,borderLeftWidth: 2}}></View>
            <Text style={[styles.titleText,this.state.color ? {color: 'black'} : {color: '#006d33'}]}>{title}</Text>
          </View>
      </TouchableHighlight>
    );
  };
}

export {Category, Category2};
