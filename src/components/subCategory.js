import React,{Component} from 'react';
import {
  Dimensions, I18nManager, Image, View, Text, TouchableHighlight, StyleSheet
} from 'react-native';
import FAIcon from 'react-native-vector-icons/FontAwesome';


const styles = StyleSheet.create({
  titleText: {
    fontSize: 16,
    color: 'black',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
    maxWidth: 200,
  },
  subtitleText: {
    fontSize: 16,
    color: 'black',
    textAlign: 'center',
    fontFamily: I18nManager.isRTL ? 'GE-SS-Two-Light' : 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'normal',
  },
  ItemContainer2: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3,
    paddingBottom: 5,
  },
  Item2: {
    width: '100%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
  },
  textContainer: {
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flex: 1,
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
  },
  iconContainer: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: 20,
  }
});


class SubCategory extends Component {
  constructor() {
    super();
    this.state = {
      color: true
    }
  }
  render() {
    const { icon,title,onPress,subtitle } = this.props;
    return (
      <TouchableHighlight underlayColor='transparent' onHideUnderlay={()=>this.setState({color:true})} onShowUnderlay={()=>this.setState({color:false})} onPress={onPress} style={styles.ItemContainer2}>
        <View style={[styles.Item2,this.state.color ? {backgroundColor: '#fee100'} : {backgroundColor: '#006d33'}]}>
          <View style={styles.iconContainer}>
            <FAIcon name="circle" size={21} color="white" />
          </View>
          <View style={styles.textContainer}>
            <Text numberOfLines={1} style={[styles.titleText,this.state.color ? {color: 'black'} : {color: 'white'}]}>{title}</Text>
            {subtitle && <Text style={[styles.subtitleText,this.state.color ? {color: 'black'} : {color: 'white'}]}>{subtitle}</Text>}
          </View>
          <View style={styles.iconContainer}>
            <FAIcon name={I18nManager.isRTL ? "chevron-left" : "chevron-right" } size={20} color="white" />
          </View>
        </View>
      </TouchableHighlight>
    );
  };
}

export default SubCategory;
