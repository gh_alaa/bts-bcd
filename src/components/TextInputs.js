import React from 'react';
import {
  I18nManager, View, TextInput, StyleSheet
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text';

const styles = StyleSheet.create({
  bcdInput: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.65)',
    backgroundColor: 'white',
    textAlign: 'center',
    fontSize: 20,
    width: '100%',
    fontFamily: 'Arial',
    fontWeight: I18nManager.isRTL ? 'normal' : 'bold',
  },
  shadow: {
    position: 'absolute',
    height: 1,
    width: '100%',
    alignSelf: 'center',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 1,
    shadowOpacity: 1,
    zIndex: 1,
  }
});

const BTextInput = (props) => {
  const { onChange, keyboard } = props;
  return (
    <View style={{ width: '85%' }}>
      <View style={[styles.shadow, {
        top: 0, height: '100%', width: 1, right: 0, backgroundColor: 'rgba(0,0,0,0.3)', shadowColor: 'rgba(0,0,0,0.3)'
      }]}
      />
      <View style={[styles.shadow, { top: 1, backgroundColor: 'rgba(0,0,0,0.3)', shadowColor: 'rgba(0,0,0,0.)' }]} />
      <TextInput
        onChangeText={onChange}
        style={styles.bcdInput}
        keyboardType={keyboard}
      />
    </View>
  );
};

const BCurruncyTextInput = (props) => {
  const {
    onChange, keyboard, value, currency
  } = props;
  return (
    <View style={{ width: '85%' }}>
      <View style={[styles.shadow, {
        top: 0, height: '100%', width: 1, right: 0, backgroundColor: 'rgba(0,0,0,0.3)', shadowColor: 'rgba(0,0,0,0.3)'
      }]}
      />
      <View style={[styles.shadow, { top: 1, backgroundColor: 'rgba(0,0,0,0.3)', shadowColor: 'rgba(0,0,0,0.)' }]} />
      <TextInputMask
        value={value}
        onChangeText={onChange}
        style={styles.bcdInput}
        keyboardType={keyboard}
        options={{
          precision: 1,
          separator: '.',
          delimiter: ',',
          unit: `${currency} `,
          suffixUnit: ''
        }}
        type="money"
      />
    </View>
  );
};

const BSecureTextInput = (props) => {
  const { onChange, keyboard } = props;
  return (
    <View style={{ width: '85%' }}>
      <View style={[styles.shadow, {
        top: 0, height: '100%', width: 1, right: 0, backgroundColor: 'rgba(0,0,0,0.3)', shadowColor: 'rgba(0,0,0,0.3)'
      }]}
      />
      <View style={[styles.shadow, { top: 1, backgroundColor: 'rgba(0,0,0,0.3)', shadowColor: 'rgba(0,0,0,0.)' }]} />
      <TextInput
        secureTextEntry
        onChangeText={onChange}
        style={styles.bcdInput}
        keyboardType={keyboard}
      />
    </View>
  );
};


export { BSecureTextInput, BCurruncyTextInput, BTextInput };
