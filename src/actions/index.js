const GetUser = user => ({
  type: 'GET_USER',
  payload: user,
});

const PopUser = () => ({
  type: 'POP_USER',
});

export { PopUser, GetUser };
