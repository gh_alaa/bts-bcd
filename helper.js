const getXmlData = (data, name) => JSON.parse(data['soap:Envelope']['soap:Body'][0][`${name}Response`][0][`${name}Result`][0]);


export default getXmlData;
